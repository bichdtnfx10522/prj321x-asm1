<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="row">
		<div class="leftcolum">
			<div class="card">
				<h2>Infomation of product 1</h2>
				<h5>Title description, Dec7, 2017</h5>
				<div class="fakeimg" style="height: 200px;">Image</div>
				<p>Some text..</p>
			</div>
			<div class="card">
				<h2>Infomation of product 2</h2>
				<h5>Title description, Dec7, 2017</h5>
				<div class="fakeimg" style="height: 200px;">Image</div>
				<p>Some text..</p>
			</div>
		</div>
		<div class="rightcolum">
			<div class="card">
				<h2>Shopping cart</h2>
				<div class="fakeimg" style="height: 100px;">Cart</div>
				<p>Summary information of your cart can be displayed here</p>
			</div>
			<div class="card">
				<h3>Popular products or banners</h3>
				<div class="fakeimg" style="margin-bottom: 1px;">
					<p>Image</p>
				</div>
				<div class="fakeimg" style="margin-bottom: 1px;">
					<p>Image</p>
				</div>
				<div class="fakeimg" style="margin-bottom: 1px;">
					<p>Image</p>
				</div>
			</div>

		</div>
	</div>
</body>
</html>