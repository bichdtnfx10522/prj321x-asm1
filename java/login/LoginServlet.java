package login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
//@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		
		// Du lieu truyen tu form
		String userID = request.getParameter("username");
		String password = request.getParameter("password");
		
		// Doc thong tin cau hinh trong file xml
		//String uid = getServletContext().getInitParameter("user");
		//String pwd = getServletContext().getInitParameter("password");
		ServletConfig config = getServletConfig();
		String uid = config.getInitParameter("user");
		String pwd = config.getInitParameter("password");
		
		if (userID !=null && password .equals(pwd)&& userID.equalsIgnoreCase(uid)) {
			response.sendRedirect("home.jsp");
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			response.getWriter().println("<p style='color:red;'>User orr password iss invalid</p>");
			rd.include(request, response);
		}
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
